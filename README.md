# role-cisco

Manage all cisco devices (Cisco Switches) via Ansible Playbook in the Hackerspace Toolbox Bodensee e.V.

 Hint:
-----

The current Cisco configuration will be automatically backuped while executing this playbook to the folder "backup" in this directory.


